from django.http import HttpResponse


def home(request):
    return HttpResponse('''The app is running!
    
    To make an admin user run `kubectl exec -it <pod name> python manage.py createsuperuser` on a pod.''')
